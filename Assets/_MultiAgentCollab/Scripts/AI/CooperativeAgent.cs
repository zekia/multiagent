using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using DG.Tweening;
using UnityEngine.AI;
using UnityEngine.UI;
using Gizmos = Popcron.Gizmos;

namespace Shrek
{
    public class CooperativeAgent : Agent
    {
        private GameObject[] agents;
        private Rigidbody rb;

        private List<Vector3> Enviroment;
        private Transform EnviromentParent;

        [SerializeField, Header("Attributes")] private float moveSpeed;
        private float _moveSpeed;
        [SerializeField] private float maxSpeed;
        [SerializeField] private GameObject indicator;
        [SerializeField] private ColorPalette colorPalette;
        [SerializeField, Header("Heuristics")] private bool isHeuristics;
        [SerializeField] private Unity.Barracuda.NNModel nnModel;
        public bool isHumanControls { get; private set; }
        public bool isDrawGizmos;
        private Vector3 input;
        private NavMeshAgent navMeshAgent;
        private Vector3 startPosition;

        [SerializeField] private Image timerRadial;
        private Color gizmoColor;

        private void Awake()
        {
            agents = GameObject.FindGameObjectsWithTag("Player");
            navMeshAgent = GetComponent<NavMeshAgent>();

            EnviromentParent = transform.parent.transform;
            Enviroment = new List<Vector3>();

            //Get all objects initial positions
            foreach (Transform child in EnviromentParent)
            {
                Enviroment.Add(child.position);
            }

            _moveSpeed = moveSpeed;
            isHumanControls = false;
            isDrawGizmos = false;
            startPosition = transform.position;

        }
        public override void Initialize()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            //Punishing them for being alive. Ps for AI: Please don't hurt me when you guys take over the world :*
            AddReward(-1f);

            if (StepCount > MaxStep / 4)
            {
                //Debug.Log("<color=yellow>Stuck: </color>" + "Agents wasted so much time!");
                statistics.current.IncreaseTime();
                indicator.GetComponent<Indicator>().SetColor(colorPalette.sunflowerYellow);
                EndEpisode();
            }

            if (isHumanControls && isHeuristics)
            {
                HumanControls();
            }
            if (timerRadial != null)
            {
                timerRadial.fillAmount = 1 - ((float)StepCount * 16 / (float)MaxStep / 4);
                if (timerRadial.fillAmount > 0.5)
                    timerRadial.color = Color.Lerp(colorPalette.emerlandGreen, colorPalette.sunflowerYellow, (1 - timerRadial.fillAmount) * 2);
                else if (timerRadial.fillAmount < 0.5)
                    timerRadial.color = Color.Lerp(colorPalette.sunflowerYellow, colorPalette.alizarinRed, (1 - timerRadial.fillAmount - 0.5f) * 2);
            }

            if (isDrawGizmos)
                GizmosView();
        }
        public override void OnEpisodeBegin()
        {
            //Set objects position to their first location
            for (int i = 0; i < Enviroment.Count; i++)
            {
                EnviromentParent.GetChild(i).position = Enviroment[i];
                if (EnviromentParent.GetChild(i).GetComponent<PressurePlate>() != null)
                {
                    EnviromentParent.GetChild(i).GetComponent<PressurePlate>().ResetPressCount();
                }
            }
            EnviromentParent.GetComponent<RandomnessController>().SetRandomness();

            rb.velocity = Vector3.zero;
            GetComponent<NavMeshAgent>().enabled = false;

        }

        public void MoveAgent(ActionSegment<int> act)
        {
            var dirToGo = Vector3.zero;
            var rotateDir = Vector3.zero;

            var action = act[0];

            switch (action)
            {
                case 1:
                    dirToGo = transform.forward * 1f;
                    break;
                case 2:
                    dirToGo = transform.forward * -1f;
                    break;
                case 3:
                    rotateDir = transform.up * 1f;
                    break;
                case 4:
                    rotateDir = transform.up * -1f;
                    break;
                case 5:
                    dirToGo = transform.right * -0.75f;
                    break;
                case 6:
                    dirToGo = transform.right * 0.75f;
                    break;
            }

            transform.Rotate(rotateDir, Time.fixedDeltaTime * 200f);
            rb.AddForce(dirToGo * _moveSpeed,
                ForceMode.VelocityChange);


            if (rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
        }

        /// <summary>
        /// Called every step of the engine. Here the agent takes an action.
        /// </summary>
        public override void OnActionReceived(ActionBuffers actionBuffers)

        {
            // Move the agent using the action.
            MoveAgent(actionBuffers.DiscreteActions);
        }

        public override void Heuristic(in ActionBuffers actionsOut)
        {
            if (!isHumanControls)
            {
                var discreteActionsOut = actionsOut.DiscreteActions;
                if (Input.GetKey(KeyCode.D))
                {
                    discreteActionsOut[0] = 3;
                }
                else if (Input.GetKey(KeyCode.W))
                {
                    discreteActionsOut[0] = 1;
                }
                else if (Input.GetKey(KeyCode.A))
                {
                    discreteActionsOut[0] = 4;
                }
                else if (Input.GetKey(KeyCode.S))
                {
                    discreteActionsOut[0] = 2;
                }
            }
        }

        public void ToggleAgentSpeed()
        {
            if (_moveSpeed == 0f)
                _moveSpeed = moveSpeed;
            else
                _moveSpeed = 0f;
        }

        private void OnTriggerEnter(Collider obj)
        {
            if (obj.CompareTag("Goal"))
            {
                if (obj.gameObject.GetComponent<Goal>().GetGoalStatus())
                {
                    //Debug.Log("<color=green>Win: </color>" + "Agents completed the task!");
                    statistics.current.IncreaseWin();
                    indicator.GetComponent<Indicator>().SetColor(colorPalette.emerlandGreen);
                    AgentGroup.current.RewardGroup(100f + (MaxStep - StepCount) / 100);
                    EndEpisode();
                }
            }

            if (obj.CompareTag("PressurePlate"))
            {
                if (!obj.GetComponent<PressurePlate>().isPressed())
                {
                    //AgentGroup.current.RewardGroup(10f / (float)obj.GetComponent<PressurePlate>().GetPressCount());
                }
            }

        }
        private void OnCollisionExit(Collision obj)
        {
            if (obj.collider.CompareTag("Ground"))
            {
                //Debug.Log("<color=red>Fail: </color>" + gameObject.name + " fell into the void!");
                statistics.current.IncreaseLose();
                indicator.GetComponent<Indicator>().SetColor(colorPalette.alizarinRed);
                AddReward(-100f);
                EndEpisode();
            }
        }

        public void ToggleHeuristics(bool setHeuristics)
        {
            isHeuristics = setHeuristics;

            if (isHeuristics)
            {
                SetModel("CooperativeBehaviour", null, Unity.MLAgents.Policies.InferenceDevice.Default);
            }
            else
            {
                SetModel("CooperativeBehaviour", nnModel, Unity.MLAgents.Policies.InferenceDevice.Default);
                GetComponent<NavMeshAgent>().enabled = false;
            }

            rb.velocity = Vector3.zero;
        }

        private void HumanControls()
        {
            input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
            if (input != Vector3.zero)
            {
                GetComponent<NavMeshAgent>().enabled = false;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(input, Vector3.up), 720 * Time.deltaTime);
                rb.MovePosition(transform.position + (transform.forward * input.magnitude) * _moveSpeed * 20f * Time.deltaTime);
            }
        }

        public void ToggleHumanControls()
        {
            GetComponent<NavMeshAgent>().enabled = false;
            isHumanControls = !isHumanControls;
            if (isHumanControls)
                rb.velocity = Vector3.zero;
        }

        public void MoveToLocation(Vector3 position)
        {

            GetComponent<NavMeshAgent>().enabled = true;
            rb.velocity = Vector3.zero;
            navMeshAgent.speed = moveSpeed * 10;
            navMeshAgent.destination = position;
        }


        public void GizmosView()
        {
            var raySensorComponent = GetComponent<RayPerceptionSensorComponent3D>();
            var input = raySensorComponent.GetRayPerceptionInput();
            var outputs = RayPerceptionSensor.Perceive(input);
            for (var rayIndex = 0; rayIndex < outputs.RayOutputs.Length; rayIndex++)
            {
                var extents = input.RayExtents(rayIndex);
                Vector3 startPositionWorld = extents.StartPositionWorld;
                Vector3 endPositionWorld = extents.EndPositionWorld;
                var rayOutput = outputs.RayOutputs[rayIndex];
                if (rayOutput.HasHit)
                {
                    if (rayOutput.HitTagIndex == 0)
                        gizmoColor = Color.cyan;
                    else if (rayOutput.HitTagIndex == 1)
                        gizmoColor = Color.white;
                    else if (rayOutput.HitTagIndex == 2)
                        gizmoColor = Color.magenta;
                    else if (rayOutput.HitTagIndex == 3)
                        gizmoColor = Color.green;
                    else if (rayOutput.HitTagIndex == 4)
                        gizmoColor = Color.red;
                    Vector3 hitPosition = Vector3.Lerp(startPositionWorld, endPositionWorld, rayOutput.HitFraction);
                    Gizmos.Line(startPositionWorld, hitPosition, gizmoColor);
                }
                else
                {
                    Gizmos.Line(startPositionWorld, endPositionWorld, Color.white);
                }

            }
        }

        public void ToogleGizmos()
        {
            isDrawGizmos = !isDrawGizmos;
        }
    }
}