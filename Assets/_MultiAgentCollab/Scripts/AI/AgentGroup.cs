using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;

namespace Shrek
{
    public class AgentGroup : MonoBehaviour
    {
        public static AgentGroup current;
        [SerializeField] private SimpleMultiAgentGroup MultiAgentGroup;
        public List<Agent> AgentsList = new List<Agent>();

        private void Awake()
        {
            current = this;
        }
        private void Start()
        {
            MultiAgentGroup = new SimpleMultiAgentGroup();
            foreach (var item in AgentsList)
            {
                MultiAgentGroup.RegisterAgent(item.GetComponent<Agent>());
            }
        }
        public void RewardGroup(float reward)
        {
            MultiAgentGroup.AddGroupReward(reward);
        }

    }
}
