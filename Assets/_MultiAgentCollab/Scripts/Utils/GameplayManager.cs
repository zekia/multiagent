using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Shrek
{
    public class GameplayManager : MonoBehaviour
    {
        [SerializeField] CooperativeAgent agent0, agent1;
        [SerializeField] GameObject enviroments;
        [SerializeField] private ColorPalette colorPalette;
        private bool heuristicAgent0, heuristicAgent1, isTimeStop, isCameraZoomIn, isEnviromentsActive;
        private Material agent0Material, agent1Material;
        [SerializeField] private Vector3 cameraAgentRotation, cameraUserRotation;
        private Ray ray;
        private RaycastHit hit;
        [SerializeField] private GameObject onClickGroundParticle, onClickPlayerParticle;

        private void Start()
        {
            agent0Material = agent0.GetComponent<MeshRenderer>().material;
            agent1Material = agent1.GetComponent<MeshRenderer>().material;

            isCameraZoomIn = true;
            isEnviromentsActive = enviroments.activeInHierarchy;
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                SetAgent0Active();
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                SetAgent1Active();
            }
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (heuristicAgent0)
                    SetAgent1Active();
                else
                    SetAgent0Active();

            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                UIManager.Instance.ToggleControlsUI();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                agent0.ToggleAgentSpeed();
                agent1.ToggleAgentSpeed();
            }

            if (Input.GetKeyDown(KeyCode.H))
            {
                agent0.ToggleHumanControls();
                agent1.ToggleHumanControls();
                ToggleHumanCamera();
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                ToggleZoom();
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                UIManager.Instance.ToggleTimer();
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                UIManager.Instance.ToggleCreditsUI();
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                isEnviromentsActive = !isEnviromentsActive;
                enviroments.SetActive(isEnviromentsActive);
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                agent0.ToogleGizmos();
                agent1.ToogleGizmos();
            }

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject == agent0.gameObject)
                    {
                        SetAgent0Active();
                    }
                    else if (hit.collider.gameObject == agent1.gameObject)
                    {
                        SetAgent1Active();
                    }


                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (Physics.Raycast(ray, out hit))
                {
                    if (heuristicAgent0)
                    {
                        agent0.MoveToLocation(hit.point);
                    }


                    else if (heuristicAgent1)
                    {
                        agent1.MoveToLocation(hit.point);
                    }

                    Instantiate(onClickGroundParticle, hit.point, onClickGroundParticle.transform.rotation);
                }
            }
        }
        private void ToggleZoom()
        {
            if (isCameraZoomIn)
                DOTween.To(() => Camera.main.orthographicSize, x => Camera.main.orthographicSize = x, 25, 1f).SetEase(Ease.OutQuad).SetUpdate(true);
            else
                DOTween.To(() => Camera.main.orthographicSize, x => Camera.main.orthographicSize = x, 7, 1f).SetEase(Ease.OutQuad).SetUpdate(true);

            isCameraZoomIn = !isCameraZoomIn;
        }

        private void ToggleHumanCamera()
        {
            if (agent0.isHumanControls || agent1.isHumanControls)
            {
                Camera.main.transform.DORotate(cameraUserRotation, 1f);
            }
            else
            {
                Camera.main.transform.DORotate(cameraAgentRotation, 1f);
            }
        }
        private void SetAgent0Active()
        {
            heuristicAgent0 = !heuristicAgent0;
            heuristicAgent1 = false;

            agent0.ToggleHeuristics(heuristicAgent0);
            agent1.ToggleHeuristics(false);
            if (heuristicAgent0)
            {

                agent0Material.DOColor(colorPalette.emerlandGreen, 0.3f).SetEase(Ease.OutCirc);

                Instantiate(onClickPlayerParticle, agent0.transform.position, onClickPlayerParticle.transform.rotation);
            }
            else
                agent0Material.DOColor(colorPalette.asbestosGrey, 0.3f).SetEase(Ease.OutCirc);
            agent1Material.DOColor(colorPalette.asbestosGrey, 0.3f).SetEase(Ease.OutCirc);
        }

        private void SetAgent1Active()
        {
            heuristicAgent1 = !heuristicAgent1;
            heuristicAgent0 = false;

            agent1.ToggleHeuristics(heuristicAgent1);
            agent0.ToggleHeuristics(false);
            if (heuristicAgent1)
            {
                agent1Material.DOColor(colorPalette.emerlandGreen, 0.3f).SetEase(Ease.OutCirc);
                Instantiate(onClickPlayerParticle, agent1.transform.position, onClickPlayerParticle.transform.rotation);
            }
            else
                agent1Material.DOColor(colorPalette.asbestosGrey, 0.3f).SetEase(Ease.OutCirc);
            agent0Material.DOColor(colorPalette.asbestosGrey, 0.3f).SetEase(Ease.OutCirc);
        }
    }
}
