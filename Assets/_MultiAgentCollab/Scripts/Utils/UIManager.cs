using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

namespace Shrek
{
    public class UIManager : MonoBehaviour
    {
        [System.Serializable]
        public class UIKey
        {
            public GameObject UIObject;
            public KeyCode keyCode;
            public CanvasGroup darkKey;
        }
        public static UIManager Instance;

        [SerializeField] private RectTransform controlsUI, line, credits, creditsBackground, creditsText, creditsLine, timer;
        [SerializeField] private List<UIKey> UIKeys;
        [SerializeField, Range(0f, 5f)] private float UIAnimationDuration;
        private Vector2 controlsUISize, creditsBackgroundSize, controlsLineSize, creditsLineSize;
        private Sequence tweenSequence, keyPressSequence;
        private string CapitalizeString;
        private bool isEsc, isSpace, isH, isZ, isV, isC, isT, isX;
        [SerializeField] TextMeshProUGUI Esc, Space, H, Z, V, C, T, X;
        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            controlsUISize = controlsUI.sizeDelta;
            creditsBackgroundSize = new Vector2(1350, 1030);
            controlsLineSize = line.sizeDelta;
            creditsLineSize = new Vector2(1080, 2.5f);

            isEsc = true;
            isSpace = true;
            isH = false;
            isZ = true;
            isV = true;
            isT = true;
        }
        private void Update()
        {
            foreach (UIKey uiKey in UIKeys)
            {
                if (Input.GetKeyDown(uiKey.keyCode))
                {
                    uiKey.darkKey.DOFade(1f, 0.2f).SetEase(Ease.OutExpo).SetUpdate(true);
                }
                if (Input.GetKeyUp(uiKey.keyCode))
                {
                    uiKey.darkKey.DOFade(0f, 0.2f).SetEase(Ease.OutExpo).SetUpdate(true);
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (isEsc)
                    foreach (UIKey uiKey in UIKeys)
                    {
                        if (uiKey.keyCode == KeyCode.Escape)
                        {
                            Esc.text = "Show Controls";
                        }
                    }
                else
                    Esc.text = "Hide Controls";

                isEsc = !isEsc;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (isSpace)
                    foreach (UIKey uiKey in UIKeys)
                    {
                        if (uiKey.keyCode == KeyCode.Space)
                        {
                            Space.text = "Enable NN Brain";
                        }
                    }
                else
                    Space.text = "Disable NN Brain";

                isSpace = !isSpace;

            }
            if (Input.GetKeyDown(KeyCode.H))
            {
                if (isH)
                    foreach (UIKey uiKey in UIKeys)
                    {
                        if (uiKey.keyCode == KeyCode.H)
                        {
                            H.text = "Enable Human Controls";
                        }
                    }
                else
                    H.text = "Disable Human Controls";

                isH = !isH;
            }
            if (Input.GetKeyDown(KeyCode.X))
            {
                if (isX)
                    foreach (UIKey uiKey in UIKeys)
                    {
                        if (uiKey.keyCode == KeyCode.X)
                        {
                            X.text = "Enable Raycast View";
                        }
                    }
                else
                    X.text = "Disable Raycast View";

                isX = !isX;
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                if (isZ)
                    foreach (UIKey uiKey in UIKeys)
                    {
                        if (uiKey.keyCode == KeyCode.Space)
                        {
                            Z.text = "Zoom In";
                        }
                    }
                else
                    Z.text = "Zoom Out";

                isZ = !isZ;

            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                if (isV)
                    foreach (UIKey uiKey in UIKeys)
                    {
                        if (uiKey.keyCode == KeyCode.V)
                        {
                            V.text = "Enable Environments";
                        }
                    }
                else
                    V.text = "Disable Environments";

                isV = !isV;
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                if (isC)
                    foreach (UIKey uiKey in UIKeys)
                    {
                        if (uiKey.keyCode == KeyCode.V)
                        {
                            C.text = "Show Credits";
                        }
                    }
                else
                    C.text = "Hide Credits";

                isC = !isC;
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                if (isT)
                    foreach (UIKey uiKey in UIKeys)
                    {
                        if (uiKey.keyCode == KeyCode.T)
                        {
                            T.text = "Hide Timer";
                        }
                    }
                else
                    T.text = "Show Timer";

                isT = !isT;
            }
        }
        public void ToggleControlsUI()
        {
            if (!isEsc)
            {
                tweenSequence.Kill();
                tweenSequence = DOTween.Sequence();
                tweenSequence = tweenSequence.Join(controlsUI.DOSizeDelta(new Vector2(controlsUISize.x, 0f), UIAnimationDuration).SetDelay(UIAnimationDuration / 5).SetEase(Ease.OutExpo))
                .Join(line.DOSizeDelta(new Vector2(0f, controlsLineSize.y), UIAnimationDuration / 2).SetEase(Ease.InBack)).SetUpdate(true);
            }
            else
            {
                tweenSequence.Kill();
                tweenSequence = DOTween.Sequence();
                tweenSequence = tweenSequence.Join(controlsUI.DOSizeDelta(controlsUISize, UIAnimationDuration).SetEase(Ease.OutExpo))
               .Join(line.DOSizeDelta(controlsLineSize, UIAnimationDuration).SetEase(Ease.OutExpo)).SetUpdate(true);
            }
        }

        public void ToggleCreditsUI()
        {
            if (!isC)
            {
                credits.DOLocalMoveY(850, UIAnimationDuration / 5).SetEase(Ease.Linear).OnComplete(() =>
                  {
                      creditsLine.DOSizeDelta(new Vector2(0f, creditsLineSize.y), UIAnimationDuration / 5).SetEase(Ease.InBack);
                      creditsBackground.DOSizeDelta(new Vector2(creditsBackgroundSize.x, 0f), UIAnimationDuration).SetEase(Ease.OutExpo).OnComplete(() =>
                      {
                          if (isT)
                          {
                              timer.DOLocalMoveY(0f, UIAnimationDuration).SetEase(Ease.OutExpo);
                          }
                      });
                  });

            }
            else
            {
                creditsLine.DOSizeDelta(creditsLineSize, UIAnimationDuration).SetEase(Ease.OutExpo).SetDelay(UIAnimationDuration / 5);
                creditsBackground.DOSizeDelta(creditsBackgroundSize, UIAnimationDuration).SetEase(Ease.OutExpo);
                credits.DOLocalMoveY(-1000, 0f);
                credits.DOLocalMoveY(-75f, UIAnimationDuration * 10).SetEase(Ease.Linear);

                if (isT)
                {
                    timer.DOLocalMoveY(150f, UIAnimationDuration / 5).SetEase(Ease.OutExpo);
                }
            }
        }

        public void ToggleTimer()
        {
            if (!isT)
            {
                timer.DOLocalMoveY(150f, UIAnimationDuration).SetEase(Ease.OutExpo);
            }
            else
                timer.DOLocalMoveY(0f, UIAnimationDuration).SetEase(Ease.OutExpo);
        }
    }
}