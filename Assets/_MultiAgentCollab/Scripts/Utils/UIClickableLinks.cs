using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

namespace Shrek
{
    public class UIClickableLinks : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] string url;
        public void OnPointerClick(PointerEventData eventData)
        {
            if (GetComponent<Image>())
                Application.OpenURL(url);

            else if (GetComponent<TextMeshProUGUI>())
            {
                int linkIndex = TMP_TextUtilities.FindIntersectingLink(GetComponent<TextMeshProUGUI>(), Input.mousePosition, null);
                if (linkIndex != -1)
                { // was a link clicked?
                    TMP_LinkInfo linkInfo = GetComponent<TextMeshProUGUI>().textInfo.linkInfo[linkIndex];

                    // open the link id as a url, which is the metadata we added in the text field
                    Application.OpenURL(linkInfo.GetLinkID());
                }

            }
        }
    }
}
