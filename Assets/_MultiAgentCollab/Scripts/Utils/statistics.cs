using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shrek
{
    public class statistics : MonoBehaviour
    {
        public static statistics current;
        public static int winCount;
        public static int loseCount;
        public static int timeoutCount;
        private void Awake()
        {
            if (current != null)
            {
                Destroy(gameObject);
            }
            else
            {
                current = this;
            }
        }
        private void Start()
        {
            winCount = 0;
            loseCount = 0;
            timeoutCount = 0;
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.J))
                print("Total test: " + (winCount + loseCount + timeoutCount) + " Win count: " + winCount + " Fail count: " + loseCount + " Stuck count: " + timeoutCount);

            if ((winCount + loseCount + timeoutCount) % 1000 == 0)
                print("Total test: " + (winCount + loseCount + timeoutCount) + " Win count: " + winCount + " Fail count: " + loseCount + " Stuck count: " + timeoutCount);

        }

        public void IncreaseWin()
        {
            winCount++;
        }
        public void IncreaseLose()
        {
            loseCount++;
        }
        public void IncreaseTime()
        {
            timeoutCount++;
        }
    }
}