using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shrek
{
    public class Indicator : MonoBehaviour
    {
        private Material material;
        private void Awake()
        {
            material = GetComponent<MeshRenderer>().material = new Material(GetComponent<MeshRenderer>().material);
        }

        public void SetColor(Color color)
        {
            material.color = color;
        }
    }
}
