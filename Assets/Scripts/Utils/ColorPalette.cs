using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shrek
{
    [CreateAssetMenu(fileName = "ColorPalette", menuName = "ScriptableObjects/ColorPalette", order = 1)]
    public class ColorPalette : ScriptableObject
    {
        public Color alizarinRed, emerlandGreen, sunflowerYellow;
    }
}
