using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Shrek
{
    public class RandomnessController : MonoBehaviour
    {
        [SerializeField] private Transform wallLeft, wallRight, pressurePlate0, pressurePlate1, door, goal;

        private float wallRandom;
        private readonly float totalWallLength = 2.0f, planeLenth = 10.0f;

        public void SetRandomness()
        {


            wallRandom = Random.Range(0f, totalWallLength);
            wallLeft.DOScaleX(wallRandom, 0f);
            wallRight.DOScaleX(2 - wallRandom, 0f);

            door.DOMoveX(door.position.x + Mathf.Lerp(-planeLenth / 2, planeLenth / 2, wallRandom / (planeLenth / 4)) + door.localScale.x / 2, 0f);

            pressurePlate0.DOMoveX(pressurePlate0.position.x + Random.Range(-(planeLenth - pressurePlate0.localScale.x) / 2, (planeLenth - pressurePlate0.localScale.x) / 2), 0f);
            pressurePlate1.DOMoveX(pressurePlate1.position.x + Random.Range(-(planeLenth - pressurePlate1.localScale.x) / 2, (planeLenth - pressurePlate1.localScale.x) / 2), 0f);
            goal.DOMoveX(goal.position.x + Random.Range(-goal.localScale.x, goal.localScale.x) / 2, 0f);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                SetRandomness();
            }
        }
    }
}
