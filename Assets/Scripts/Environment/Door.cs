using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Shrek
{
    public class Door : MonoBehaviour
    {
        private Sequence doorSequence;
        private Material material;
        [SerializeField] ColorPalette colorPalette;
        private float initialPositionY;

        private void Awake()
        {

            material = GetComponent<MeshRenderer>().material = new Material(GetComponent<MeshRenderer>().material);

            doorSequence = DOTween.Sequence();

            initialPositionY = transform.position.y;
        }
        public void OpenDoor()
        {
            doorSequence.Join(transform.DOMoveY(initialPositionY + transform.localScale.y, 0.0f))
            .Join(material.DOColor(colorPalette.emerlandGreen, 0.0f));
        }
        public void CloseDoor(Transform plate)
        {

            //Close the door only if no pressure plates are active
            if (!plate.GetComponent<PressurePlate>().isPressed())
            {
                doorSequence.Join(transform.DOMoveY(initialPositionY, 0.0f))
                .Join(material.DOColor(colorPalette.alizarinRed, 0.0f));

            }
        }
    }
}
