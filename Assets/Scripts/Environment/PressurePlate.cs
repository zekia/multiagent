using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Shrek
{
    public class PressurePlate : MonoBehaviour
    {
        [SerializeField] private UnityEvent OnPressEnter = new UnityEvent();
        [SerializeField] private UnityEvent OnPressExit = new UnityEvent();
        private int pressingCount, totalPressingCount;

        private Material material;
        [SerializeField] private ColorPalette colorPalette;

        private void Awake()
        {
            material = GetComponent<MeshRenderer>().material = new Material(GetComponent<MeshRenderer>().material);

            pressingCount = 0;
            totalPressingCount = 1;
        }

        private void OnTriggerEnter(Collider obj)
        {
            if (obj.CompareTag("Player"))
            {
                StartCoroutine(Pressed());
                OnPressEnter.Invoke();
                material.color = colorPalette.emerlandGreen;
            }
        }

        IEnumerator Pressed()
        {
            yield return new WaitForFixedUpdate();
            pressingCount++;
            totalPressingCount++;
        }
        private void OnTriggerExit(Collider obj)
        {
            if (obj.CompareTag("Player"))
            {
                pressingCount--;
                if (pressingCount == 0)
                {

                    OnPressExit.Invoke();
                    material.color = colorPalette.alizarinRed;
                }
            }
        }

        public bool isPressed()
        {
            return pressingCount == 0 ? false : true;
        }
        public int GetPressCount()
        {
            return totalPressingCount;
        }
        public void ResetPressCount()
        {
            totalPressingCount = 1;
        }
    }
}
