using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace Shrek
{
    public class Goal : MonoBehaviour
    {

        [System.Serializable]
        public class TriggerEvent : UnityEvent<Collider>
        {
        }
        [Header("Trigger Callbacks")]
        public TriggerEvent onTriggerEnterEvent = new TriggerEvent();
        public TriggerEvent onTriggerStayEvent = new TriggerEvent();
        public TriggerEvent onTriggerExitEvent = new TriggerEvent();


        private Material material;
        [SerializeField] ColorPalette colorPalette;
        private List<Color> colors;
        private int succesfullPlayerCount = 0, totalPlayerCount;
        private void Awake()
        {
            //totalPlayerCount = GameObject.FindGameObjectsWithTag("Player").Length;

            material = GetComponent<MeshRenderer>().material = new Material(GetComponent<MeshRenderer>().material);

            colors = new List<Color>();
            colors.Add(colorPalette.alizarinRed);
            colors.Add(colorPalette.sunflowerYellow);
            colors.Add(colorPalette.emerlandGreen);
        }

        private void CheckGoal()
        {
            SetColor();
        }

        private void SetColor()
        {
            if (succesfullPlayerCount == 0)
                material.color = colorPalette.alizarinRed;
            else if (succesfullPlayerCount == 2)
                material.color = colorPalette.emerlandGreen;
            else
                material.color = colorPalette.sunflowerYellow;
        }
        private void OnTriggerEnter(Collider obj)
        {
            if (obj.CompareTag("Player"))
            {
                succesfullPlayerCount++;
                CheckGoal();
                onTriggerEnterEvent.Invoke(obj);
            }
        }

        private void OnTriggerExit(Collider obj)
        {
            if (obj.CompareTag("Player"))
            {
                succesfullPlayerCount--;
                CheckGoal();
            }
        }

        public bool GetGoalStatus()
        {
            return succesfullPlayerCount == 2;
        }
    }
}
