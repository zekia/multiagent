using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using DG.Tweening;

namespace Shrek
{
    public class CooperativeAgent : Agent
    {
        private GameObject[] agents;
        private Rigidbody rb;

        private List<Vector3> Enviroment;
        private Transform EnviromentParent;

        [SerializeField, Header("Attributes")] private float moveSpeed;
        [SerializeField] private float maxSpeed;
        [SerializeField] private GameObject indicator;
        [SerializeField] private ColorPalette colorPalette;
        [SerializeField, Header("Heuristics")] private bool isHeuristics;
        [SerializeField] private Vector3 cameraAgentRotation, cameraUserRotation;

        private void Awake()
        {
            agents = GameObject.FindGameObjectsWithTag("Player");

            EnviromentParent = transform.parent.transform;
            Enviroment = new List<Vector3>();

            //Get all objects initial positions
            foreach (Transform child in EnviromentParent)
            {
                Enviroment.Add(child.position);
            }
        }
        public override void Initialize()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            //Punishing them for being alive. Ps for AI: Please don't hurt me when you guys take over the world :*
            AddReward(-1f);

            if (StepCount > MaxStep / 4)
            {
                Debug.Log("<color=yellow>Stuck: </color>" + "Agents wasted so much time!");
                indicator.GetComponent<Indicator>().SetColor(colorPalette.sunflowerYellow);
                EndEpisode();
            }
        }
        public override void OnEpisodeBegin()
        {
            //Set objects position to their first location
            for (int i = 0; i < Enviroment.Count; i++)
            {
                EnviromentParent.GetChild(i).position = Enviroment[i];
                if (EnviromentParent.GetChild(i).GetComponent<PressurePlate>() != null)
                {
                    EnviromentParent.GetChild(i).GetComponent<PressurePlate>().ResetPressCount();
                }
            }
            EnviromentParent.GetComponent<RandomnessController>().SetRandomness();
            SetCamera();

            rb.velocity = Vector3.zero;
        }

        public void MoveAgent(ActionSegment<int> act)
        {
            var dirToGo = Vector3.zero;
            var rotateDir = Vector3.zero;

            var action = act[0];

            switch (action)
            {
                case 1:
                    dirToGo = transform.forward * 1f;
                    break;
                case 2:
                    dirToGo = transform.forward * -1f;
                    break;
                case 3:
                    rotateDir = transform.up * 1f;
                    break;
                case 4:
                    rotateDir = transform.up * -1f;
                    break;
                case 5:
                    dirToGo = transform.right * -0.75f;
                    break;
                case 6:
                    dirToGo = transform.right * 0.75f;
                    break;
            }

            transform.Rotate(rotateDir, Time.fixedDeltaTime * 200f);
            rb.AddForce(dirToGo * moveSpeed,
                ForceMode.VelocityChange);


            if (rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
        }

        /// <summary>
        /// Called every step of the engine. Here the agent takes an action.
        /// </summary>
        public override void OnActionReceived(ActionBuffers actionBuffers)

        {
            // Move the agent using the action.
            MoveAgent(actionBuffers.DiscreteActions);
        }

        public override void Heuristic(in ActionBuffers actionsOut)
        {
            var discreteActionsOut = actionsOut.DiscreteActions;
            if (Input.GetKey(KeyCode.D))
            {
                discreteActionsOut[0] = 3;
            }
            else if (Input.GetKey(KeyCode.W))
            {
                discreteActionsOut[0] = 1;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                discreteActionsOut[0] = 4;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                discreteActionsOut[0] = 2;
            }
        }

        private void OnTriggerEnter(Collider obj)
        {
            if (obj.CompareTag("Goal"))
            {
                if (obj.gameObject.GetComponent<Goal>().GetGoalStatus())
                {
                    Debug.Log("<color=green>Win: </color>" + "Agents completed the task!");
                    indicator.GetComponent<Indicator>().SetColor(colorPalette.emerlandGreen);
                    AgentGroup.current.RewardGroup(100f + (MaxStep - StepCount) / 100);
                    EndEpisode();
                }
            }

            if (obj.CompareTag("PressurePlate"))
            {
                if (!obj.GetComponent<PressurePlate>().isPressed())
                {
                    //AgentGroup.current.RewardGroup(10f / (float)obj.GetComponent<PressurePlate>().GetPressCount());
                }
            }

        }
        private void OnCollisionExit(Collision obj)
        {
            if (obj.collider.CompareTag("Ground"))
            {
                Debug.Log("<color=red>Fail: </color>" + gameObject.name + " fell into the void!");
                indicator.GetComponent<Indicator>().SetColor(colorPalette.alizarinRed);
                AddReward(-100f);
                EndEpisode();
            }
        }

        public void ToggleHeuristics()
        {
            isHeuristics = !isHeuristics;
            SetCamera();
        }

        private void SetCamera()
        {
            if (isHeuristics)
            {
                Camera.main.transform.DORotate(cameraUserRotation, 1f);
            }
            else if (!isHeuristics)
            {
                Camera.main.transform.DORotate(cameraAgentRotation, 1f);
            }
        }
    }
}